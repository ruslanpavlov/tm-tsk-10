package ru.tsc.pavlov.tm.api.repository;

import ru.tsc.pavlov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

}
