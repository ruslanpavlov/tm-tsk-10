package ru.tsc.pavlov.tm.api.service;

import ru.tsc.pavlov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
