package ru.tsc.pavlov.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

}
