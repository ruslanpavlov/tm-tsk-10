package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.api.repository.ICommandRepository;
import ru.tsc.pavlov.tm.constant.ArgumentConst;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.model.Command;

public class CommandRepository implements ICommandRepository {
    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, " Display system information"
    );
    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null, " Close application"
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, " Display developer info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, " Display program info"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, " Display list of commands"
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, " Display list arguments"
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, " Display list commands"
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Display list of projects"
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project"
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Drop all projects"
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "List tasks"
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task"
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Drop all tasks"
    );


    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, HELP, VERSION, COMMANDS, ARGUMENTS, PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_LIST, TASK_CREATE, TASK_CLEAR, EXIT
    };


    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
