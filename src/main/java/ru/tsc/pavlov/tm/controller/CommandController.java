package ru.tsc.pavlov.tm.controller;

import ru.tsc.pavlov.tm.api.controller.ICommandController;
import ru.tsc.pavlov.tm.api.service.ICommandService;
import ru.tsc.pavlov.tm.model.Command;
import ru.tsc.pavlov.tm.util.NumberUtil;


public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {

        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Ruslan Pavlov");
        System.out.println("E-MAIL: o000.ruslan@yandex.ru");

    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.9.0");
    }

    public void ShowErrorArgument() {
        System.err.println("Error! Argument not supported.");
    }

    public void ShowErrorCommand() {
        System.err.println("Error! Command not found.");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            showCommandValue(command.getName());
        }

    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            showCommandValue(command.getArgument());
        }

    }

    private void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    @Override
    public void info() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
