package ru.tsc.pavlov.tm.controller;

import ru.tsc.pavlov.tm.api.controller.ITaskController;
import ru.tsc.pavlov.tm.api.service.ITaskService;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }


    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task : tasks)
            System.out.println(task);
        System.out.println("[OK]");

    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");

    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");

    }

}
